//
//  Constent.swift
//  AnalyticsVidya
//
//  Created by Vinit Chaoudhary on 04/12/17.
//  Copyright © 2017 Vikas. All rights reserved.
//

import Foundation

enum driveUrl{
    static let downloadUrl = "https://www.googleapis.com/drive/v3/files/"
    static let fileName = "AnalyticsVidya001.json"
    static let folderName = "AnalyticsVidya"
    static let mimeType = "application/json"
    static let folderMime = "application/vnd.google-apps.folder"
    static let pageSize = 1000
    static let query = "mimeType='application/vnd.google-apps.folder' and 'root' in parents and trashed=false"
}

var drive : [driveJson] = NSMutableArray() as! [driveJson]
var indexValue = Int()
