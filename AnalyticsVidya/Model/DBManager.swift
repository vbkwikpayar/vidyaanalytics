//
//  DBManager.swift
//  AnalyticsVidya
//
//  Created by VIKAS BHATT on 04/12/17.
//  Copyright © 2017 Vikas. All rights reserved.
//

import UIKit
import CoreData
class DBManager: NSObject {
    static let shared = DBManager()
    
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    var managedContext = NSManagedObjectContext()
    
    override init(){
        super.init()
        if #available(iOS 10.0, *) {
            self.managedContext = CoreDataModel.shared.persistentContainer.viewContext
        } else {
            self.managedContext = CoreDataModel.shared.managedObjectContext
        }
    }
    
    func SaveDB(){
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func saveNotes(json : [String : Any]){
          let _driveJson = try! driveJson.init(json: json)
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Notes")
            let predicate = NSPredicate(format: "id == %@", NSNumber(value: _driveJson.id))
            request.predicate = predicate
            request.fetchLimit = 1
         do{
            let count = try managedContext.count(for: request)
            if(count > 0){
                // update 
                let data =  try managedContext.fetch(request) as! [Notes]
                let notes = data.first
                notes?.title = _driveJson.detail.title
                notes?.greeting = _driveJson.detail.greeting
                self.SaveDB()
            }
            else{
                let entity = NSEntityDescription.entity(forEntityName: "Notes",  in: managedContext)!
                let object = Notes(entity: entity,  insertInto: managedContext)
                object.greeting = _driveJson.detail.greeting
                object.title = _driveJson.detail.title
                object.id = _driveJson.id
                self.SaveDB()
            }
        }
        catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
       
    }
    
    func fetchSaveNotes() -> [Notes]{
        var notes = [Notes]()
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Notes")
        do {
            notes = try managedContext.fetch(fetchRequest) as! [Notes]
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return notes
    }
    
}
