//
//  Struct.swift
//  AnalyticsVidya
//
//  Created by VIKAS BHATT on 04/12/17.
//  Copyright © 2017 Vikas. All rights reserved.
//

import Foundation

var jsonData : NSMutableArray = [[ "id":0,"title" : "note",
                 "greeting": "",
                 ]]

struct driveJson {
    let detail :(title :String, greeting:String)
    let id : Int64
}

extension driveJson{
    init(json : [String : Any] ) throws {
        let title = json["title"] as! String
        let greeting = json["greeting"] as! String
        self.id =  json["id"] is Int64 ? json["id"] as! Int64 : Int64(json["id"] as! Int)
        self.detail = (title,greeting)
    }
}
