//
//  driveApiClient.swift
//  AnalyticsVidya
//
//  Created by VIKAS BHATT on 04/12/17.
//  Copyright © 2017 Vikas. All rights reserved.
//

import UIKit
import GoogleAPIClientForREST
import GoogleSignIn
class driveApiClient: NSObject {
    //, GIDSignInDelegate, GIDSignInUIDelegate
    private let scopes = [kGTLRAuthScopeDrive]
    
    let service = GTLRDriveService()
    let signInButton = GIDSignInButton()
    var folderIdentifire = String()
    var fileIdentifire = String()
    static let shard = driveApiClient()
    
    
    /**
     * get the list of root folder in the google Drive
     * check the folder of our app inside the drive
     * if folder does not exist then create the folder
     * other wise the the content of the json file and load into the ui
     */
    
    func loadRootFolderOfGoogleDrive(handerl:@escaping ((_ error:Error?,_ status : Bool?)->Void)){
        var checkFolderExist = false
        let query = GTLRDriveQuery_FilesList.query()
        query.pageSize = driveUrl.pageSize
        query.q = driveUrl.query;
        service.executeQuery(query) { (ticket, result, error) in
            if error == nil{
                let resultent = result as! GTLRDrive_FileList
                if let files = resultent.files, !files.isEmpty {
                    for file in files {
                        if ( file.name ==  driveUrl.folderName){
                            checkFolderExist = true
                            self.folderIdentifire = file.identifier!
                            break
                        }
                    }
                }
                
                handerl(error,checkFolderExist)
            }
            else{
                handerl(error,false)
            }
        }
    }
    
    
    /**
     * check the json file indside folder
     
     */
    
    func checkJsonFileInsideFolder(complition:@escaping ((_ error:Error?,_ file : GTLRDrive_File?)->Void))  {
        let query = GTLRDriveQuery_FilesList.query()
        query.spaces = "drive"
        query.q = "mimeType='application/json'  and trashed=false";
        service.executeQuery(query) { (ticket, result , error) in
            DispatchQueue.main.async {
                if error == nil{
                    let folder = result as? GTLRDrive_FileList
                    if let files = folder?.files, !files.isEmpty {
                        var notfound : Bool = false
                        for file in files {
                            if file.name == driveUrl.fileName{
                                self.fileIdentifire = file.identifier!
                                notfound = true
                                complition(nil,file)
                                break
                            }
                        }
                        if notfound == false{
                            complition(nil,nil)
                        }
                    }else{
                        complition(nil,nil)
                    }
                }
                else{
                    complition(error,nil)
                    print(error?.localizedDescription as Any)
                }
            }
        }
    }
    
    /**
     * create a folder in google Drive
     * that foler has the name of the app and created top of the root file
     */
    
    func CreatefolderInGoogleDrive(complition:@escaping ((_ error:Error?,_ file : GTLRDrive_File?)->Void)){
        let metadata = GTLRDrive_File()
        metadata.name = driveUrl.folderName
        metadata.mimeType = driveUrl.folderMime
        let querys = GTLRDriveQuery_FilesCreate.query(withObject: metadata, uploadParameters: nil)
        querys.fields = "id"
        self.service.executeQuery(querys) { (ticket, file, error) in
            if error == nil {
                complition(nil,file! as? GTLRDrive_File)
                // self.insertFileInfolder(identifer: (file! as! GTLRDrive_File).identifier!)
            }
            else{
                complition(error,nil)
            }
        }
    }
    
    /**
     * Download the content of the  json File which is alredy
     * created in google Drive
     */
    
    func downloadFile(file: GTLRDrive_File, handler:@escaping ((_ error:Error?,_ jsonArray : NSArray?)->Void)){
        let url = "\(driveUrl.downloadUrl)\(file.identifier!)?alt=media"
        let fetcher = service.fetcherService.fetcher(withURLString: url)
        fetcher.beginFetch { (data, error) in
            if error == nil{
                let jsonDict = try! JSONSerialization.jsonObject(with: (data as! Data), options: JSONSerialization.ReadingOptions.allowFragments)
                print(jsonDict)
                handler(nil,(jsonDict as! NSArray))
            }
            else{
                print(error?.localizedDescription as Any)
                handler(error,nil)
            }
        }
    }
    
    /**
     * create file inside the folder which is created as name of AnalyticsVidya
     *
     */
    
    func insertFileInfolder(identifer : String,complition:@escaping ((_ error:Error?,_ file : GTLRDrive_File?)->Void)){
        let file_Data = try? JSONSerialization.data(withJSONObject: jsonData, options: JSONSerialization.WritingOptions(rawValue: 0))
        let metadata = GTLRDrive_File()
        metadata.name = driveUrl.fileName
        metadata.parents = [identifer]
        let uploadparams = GTLRUploadParameters.init(data: file_Data! as Data, mimeType: driveUrl.mimeType)
        uploadparams.shouldUploadWithSingleRequest = true
        let querys = GTLRDriveQuery_FilesCreate.query(withObject: metadata, uploadParameters: uploadparams)
        querys.fields = "id"
        self.service.executeQuery(querys) { (ticket, file, error) in
            if error == nil {
                complition(nil,file as? GTLRDrive_File)
            }
            else{
                complition(error,nil)
            }
        }
    }
    
    
    /**
     * Update the jsonFile With new values
     * created in google Drive
     */
    
    func updateJsonFile(complition:@escaping ((_ error:Error?,_ status : Bool?)->Void)){
        
      let dict = NSMutableArray()
        for i in 0..<DBManager.shared.fetchSaveNotes().count{
            let data = DBManager.shared.fetchSaveNotes()[i]
           let dataValue =  [ "id":data.id as Any ,"title" : data.title as Any,
              "greeting": data.greeting as Any,
              ]
            dict.add(dataValue)
            }
        print(dict)
        
        let file_Data = try? JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions(rawValue: 0))
        let metadata = GTLRDrive_File()
        metadata.name = driveUrl.fileName
        let uploadparams = GTLRUploadParameters.init(data: file_Data! as Data, mimeType: driveUrl.mimeType)
        uploadparams.shouldUploadWithSingleRequest = true
        let querys = GTLRDriveQuery_FilesUpdate.query(withObject: metadata, fileId: self.fileIdentifire, uploadParameters: uploadparams)
        querys.fields = "id"
        querys.addParents = self.folderIdentifire
        querys.keepRevisionForever = false
        self.service.executeQuery(querys) { (ticket, file, error) in
            if error == nil {
             complition(nil,true)
            }
            else{
              complition(error,nil)
            }
            print(error?.localizedDescription)
        }
    }
    
    
    
    
}
