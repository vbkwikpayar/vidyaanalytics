//
//  UIManager.swift
//  AnalyticsVidya
//
//  Created by Vinit Chaoudhary on 04/12/17.
//  Copyright © 2017 Vikas. All rights reserved.
//

import UIKit
import GoogleSignIn
class UIManager: NSObject {
    static let shared = UIManager()
    
    // set constraint for google button
    func signInButtonToView(_ signInButton: GIDSignInButton , view : UIView) {
        signInButton.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(signInButton)
        view.addConstraints(
            [NSLayoutConstraint(item: signInButton,
                                attribute: .centerY,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerY,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: signInButton,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: signInButton,
                                attribute: .width,
                                relatedBy: .equal,
                                toItem: nil,
                                attribute: .width,
                                multiplier: 1,
                                constant: 250)
                
            ])
        
    }
    
    // Helper for showing an alert
    func showAlert(title : String, message: String ,viewController : UIViewController) {
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: UIAlertControllerStyle.alert
        )
        let ok = UIAlertAction(
            title: "OK",
            style: UIAlertActionStyle.default,
            handler: nil
        )
        alert.addAction(ok)
        viewController.present(alert, animated: true, completion: nil)
    }
}
