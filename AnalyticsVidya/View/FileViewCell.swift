//
//  FileViewCell.swift
//  AnalyticsVidya
//
//  Created by VIKAS BHATT on 04/12/17.
//  Copyright © 2017 Vikas. All rights reserved.
//

import UIKit

class FileViewCell: UITableViewCell {
    @IBOutlet var _textBox: UITextField!
    @IBOutlet var _title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
