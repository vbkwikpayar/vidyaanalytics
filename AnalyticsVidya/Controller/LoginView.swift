//
//  LoginView.swift
//  AnalyticsVidya
//
//  Created by VIKAS BHATT on 04/12/17.
//  Copyright © 2017 Vikas. All rights reserved.
//

import UIKit
import GoogleAPIClientForREST
import GoogleSignIn
class LoginView: UIViewController , GIDSignInDelegate, GIDSignInUIDelegate {
    
    
    @IBOutlet var _loader: UIActivityIndicatorView!
    @IBOutlet var _imageView: UIImageView!
    // If modifying these scopes, delete your previously saved credentials by
    // resetting the iOS simulator or uninstall the app.
    private let scopes = [kGTLRAuthScopeDrive]
    let signInButton = GIDSignInButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Configure Google Sign-in.
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().scopes = scopes
        GIDSignIn.sharedInstance().signInSilently()
        UIManager.shared.signInButtonToView(signInButton, view: self.view)
        self.signInButton.isHidden = true
        DispatchQueue.main.async {
            self._loader.startAnimating()
        }
       
    }
    
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            print(error.localizedDescription)
           // UIManager.shared.showAlert(title: "Authentication Error", message: error.localizedDescription, viewController: self)
            
            DispatchQueue.main.async {
                self.signInButton.isHidden = false
                self._imageView.isHidden = true
                self._loader.stopAnimating()
            }
            
            //self.service.
            driveApiClient.shard.service.authorizer = nil
        } else {
          
            driveApiClient.shard.service.authorizer = user.authentication.fetcherAuthorizer()
            driveApiClient.shard.loadRootFolderOfGoogleDrive(handerl: { (error, status) in
                DispatchQueue.main.async {
                    if error == nil{
                        self.signInButton.isHidden = true
                          self._imageView.isHidden = false
                        if status == false {
                            self.CreatefolderInGoogleDrive()
                        }
                        else{
                            self.checkJsonFile()
                        }
                    }
                    else{
                        self.signInButton.isHidden = false
                        UIManager.shared.showAlert(title: "Error", message: (error?.localizedDescription)!, viewController: self)
                    }
                }
            })
        }
    }
    
    //** check the json file inside the google drive ***//
    func checkJsonFile(){
        driveApiClient.shard.checkJsonFileInsideFolder { (error, file) in
            DispatchQueue.main.async {
                if error == nil {
                    // Check the file if found or not
                    if file != nil{
                        self.downloadFileContent(file: file!)
                    }
                    else{
                        // insert jsonFile to goole drive
                        driveApiClient.shard.insertFileInfolder(identifer: driveApiClient.shard.folderIdentifire, complition: { (error, file) in
                            DispatchQueue.main.async {
                                if error == nil{
                                    self.downloadFileContent(file: file!)
                                }
                                else{
                                    UIManager.shared.showAlert(title: "Error", message: (error?.localizedDescription)!, viewController: self)
                                }
                            }
                        })
                    }
                }
                else{
                    UIManager.shared.showAlert(title: "Error", message: (error?.localizedDescription)!, viewController: self)
                }
            }
        }
    }
    
    
    // download the file content from url
    func downloadFileContent(file : GTLRDrive_File){
        driveApiClient.shard.downloadFile(file: file, handler: { (error, json) in
            DispatchQueue.main.async {
                if error == nil{
                    for case let dict as [String : Any] in json!{
                        DBManager.shared.saveNotes(json: dict)
                    }
                    let viewObject : FileViewController = FileViewController(nibName: "FileViewController", bundle: nil)
                    self.navigationController?.pushViewController(viewObject, animated: true)
                }
                else{
                  UIManager.shared.showAlert(title: "Error", message: (error?.localizedDescription)!, viewController: self)
                    
                }
            }
        })
    }
    
    // Create folder inside the google drive
    func CreatefolderInGoogleDrive(){
        driveApiClient.shard.CreatefolderInGoogleDrive { (error, file) in
            DispatchQueue.main.async {
                if error == nil{
                    driveApiClient.shard.insertFileInfolder(identifer: (file?.identifier!)!, complition: { (error, file) in
                        DispatchQueue.main.async {
                            if error == nil{
                                self.downloadFileContent(file: file!)
                            }
                            else{
                                UIManager.shared.showAlert(title: "Error", message: (error?.localizedDescription)!, viewController: self)
                            }
                        }
                    })
                }
                else{
                 UIManager.shared.showAlert(title: "Error", message: (error?.localizedDescription)!, viewController: self)
                }
            }
        }
    }
}



