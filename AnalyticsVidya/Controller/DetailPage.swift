//
//  DetailPage.swift
//  AnalyticsVidya
//
//  Created by Vinit Chaoudhary on 04/12/17.
//  Copyright © 2017 Vikas. All rights reserved.
//

import UIKit

class DetailPage: UIViewController,UITableViewDataSource , UITableViewDelegate,UITextFieldDelegate  {
    
    @IBOutlet var _tableView: UITableView!
    var data : Notes?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Edit"
        _tableView.delegate = self
        _tableView.dataSource = self
        _tableView.tableFooterView = UIView(frame: CGRect.zero)
        _tableView.estimatedRowHeight = 44
        _tableView.rowHeight = UITableViewAutomaticDimension
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "FileViewCell"
        var  Cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? FileViewCell
        if Cell == nil {
            // register tableView cell
            tableView.register(UINib(nibName: "FileViewCell", bundle: nil), forCellReuseIdentifier: identifier)
            Cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? FileViewCell
        }
        let dataKey = (jsonData[0] as! NSDictionary).allKeys
        
        Cell?._title.isHidden = true
        Cell?._textBox.placeholder = dataKey[indexPath.row] as? String
        if indexPath.row == 0{
            Cell?._textBox.text = data?.title
        }
        else{
            Cell?._textBox.text = data?.greeting
        }
        Cell?._textBox.tag = indexPath.row
        Cell?._textBox.delegate = self
        Cell?.selectionStyle = .none
        return Cell!
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        var dict = NSMutableDictionary()
        switch textField.tag {
        case 0:
            dict = [ "id":data?.id as Any ,"title" : textField.text!,
                     "greeting": data?.greeting as Any,
                     ]
        case 1:
            dict = [ "id":data?.id as Any ,"title" : data?.title as Any,
                     "greeting": textField.text!,
            ]
        default:break
            
        }
        DBManager.shared.saveNotes(json: dict as! [String : Any])
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
        driveApiClient.shard.updateJsonFile() { (error, status) in
            DispatchQueue.main.async {
                if error == nil{
                    print("succes")
                }
                else{
                    print(error as Any)
                }
            }
        }
    }
   
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
