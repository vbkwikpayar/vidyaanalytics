//
//  FileViewController.swift
//  AnalyticsVidya
//
//  Created by VIKAS BHATT on 04/12/17.
//  Copyright © 2017 Vikas. All rights reserved.
//

import UIKit

class FileViewController: UIViewController,UITableViewDataSource , UITableViewDelegate {
    
    @IBOutlet var _tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Notes"
        _tableView.delegate = self
        _tableView.dataSource = self
        _tableView.tableFooterView = UIView(frame: CGRect.zero)
        _tableView.estimatedRowHeight = 44
        _tableView.rowHeight = UITableViewAutomaticDimension
          navigationItem.hidesBackButton = true
        let menuname : UIBarButtonItem = UIBarButtonItem(title: "Add", style: UIBarButtonItemStyle.plain, target: self, action: #selector(addTappedMenuBtn))
        self.navigationItem.rightBarButtonItem = menuname


    }
    @objc func addTappedMenuBtn(){
       let dict = ["id": DBManager.shared.fetchSaveNotes().count + 1 ,"title" : "Note \(DBManager.shared.fetchSaveNotes().count + 1)" ,
        "greeting": "add your greeting"] as [String : Any]
         DBManager.shared.saveNotes(json: dict)
        _tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        _tableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - tableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return DBManager.shared.fetchSaveNotes().count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "FileViewCell"
        var  Cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? FileViewCell
        if Cell == nil {
            // register tableView cell
            tableView.register(UINib(nibName: "FileViewCell", bundle: nil), forCellReuseIdentifier: identifier)
            Cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? FileViewCell
        }
        Cell?._textBox.isHidden = true
        let data = DBManager.shared.fetchSaveNotes()[indexPath.section]
        Cell?._title.text = "\(data.title ?? "")  \n\n\(String(describing: data.greeting!))"
        Cell?.selectionStyle = .none
        return Cell!
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let viewObject : DetailPage = DetailPage(nibName: "DetailPage", bundle: nil)
        viewObject.data = DBManager.shared.fetchSaveNotes()[indexPath.section]
        self.navigationController?.pushViewController(viewObject, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
        driveApiClient.shard.updateJsonFile() { (error, status) in
            DispatchQueue.main.async {
                if error == nil{
                    print("sucees")
                }
                else{
                    print(error as Any)
                }
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
